const formidable = require( "formidable" ) ;  
const fs = require( "fs" ) ;          
const { request } = require("http");

const WWW_ROOT = "www" ;
const FILE_404 = WWW_ROOT + "/404.html" ;
const DEFAULT_MIME = "application/octet-stream" ;
const UPLOAD_PATH  = WWW_ROOT + "/pictures/"

module.exports = {
    analyze: function( request, response ) {
        response.setHeader('Access-Control-Allow-Origin', '*');
        const method = request.method.toUpperCase() ;
        switch( method ) {
            case 'GET'  : 
                doGet( request, response ) ;
                break ;
            case 'POST' :  
                doPost( request, response ) ;
                break ;
            case 'DELETE' :  
                doDelete( request, response ) ;
                break ;
            case 'PUT' :  
                doPut( request, response ) ;
                break ;
            case 'OPTIONS' :  
                doOptions( request, response ) ;
                break ;
        }
    }
} ;

function doOptions( request, response ) {
    response.setHeader('Allow', 'OPTIONS, GET, POST, PUT, DELETE' ) ;
    response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE' ) ;
    response.setHeader('Access-Control-Allow-Headers', 'Content-Type' ) ;
    response.end();
}

function doPut( request, response ) {
    extractBody( request )
    .then( validateOrm )
    .then( updatePicture )
    .then( results => {
        response.setHeader( 'Content-Type', 'application/json' ) ;
        response.end( JSON.stringify( { "result": results.affectedRows } ) ) ;
    } )
    .catch( err => { console.log( err ) ; response.errorHandlers.send412( err ) ; } ) ;
}

function doDelete( request, response ) {
    extractBody( request )
    .then( validateId )
    .then( deletePicture )  
    .then( results => {
        response.setHeader( 'Content-Type', 'application/json' ) ;
        response.end( JSON.stringify( { "result": results.affectedRows } ) ) ;
    } )
    .catch( err => {
        console.log( err ) ;
        response.errorHandlers.send500() ;
    } ) ;
}

function doPost( request, response ) {
    const formParser = formidable.IncomingForm() ;
    
    formParser.parse( 
        request, 
        (err, fields, files) => {
            if( err ) {
                console.error( err ) ;
                response.errorHandlers.send500() ;
                return ;
            }

            let validateRes = validatePictureForm( fields, files ) ;
            if( validateRes === true ) {
                var oldpath = files.picture.filepath;
                console.log(oldpath);
                const savedName = moveUploadedFile( files.picture) ;
                addPicture( {
                    title:       fields.title,
                    description: fields.description,
                    place:       fields.place,
                    filename:    savedName,
                    "users_id":  fields["users_id"]
                }, request.services )
                .then( results => {

                    res = { status: results.affectedRows } ;
                    response.setHeader( 'Content-Type', 'application/json' ) ;
                    response.end( JSON.stringify( res ) ) ;

                } )
                .catch( err => {
                    console.error( err ) ;
                    response.errorHandlers.send500() ;

                } ) ;
            } else {
                response.errorHandlers.send412( validateRes ) ;
                return ;
            }
        } ) ;  
} ;

function doGet( request, response ) {
    var conditions = " WHERE "
    var queryParams = [] ;
    if( typeof request.params.query.deleted == 'undefined' ) {
        conditions += " p.delete_DT IS NULL" ;
    } else {
        conditions += " p.delete_DT IS NOT NULL" ;
    }
    if( typeof request.params.query.userid != 'undefined' ) { 
        conditions += " AND p.users_id = ? " ;
        queryParams.push( request.params.query.userid ) ;
    } else if( typeof request.params.query.exceptid != 'undefined' ) {  
        conditions += " AND ( p.users_id <> ? OR p.users_id IS NULL ) " ;
        queryParams.push( request.params.query.exceptid ) ;
    }
    const cntQuery = "SELECT COUNT(*) AS cnt FROM pictures p " + conditions ;
    request.services.dbPool.query( 
        cntQuery,
        queryParams,
        ( err, results ) => {
            if( err ) {
                console.log( err ) ;
                response.errorHandlers.send500() ;
            } else {
                const totalCnt = results[0].cnt ;  
                const perPage = 4 ;            
                const pageNumber = request.params.query.page ?? 1 ;
                const lastPage = Math.ceil( totalCnt / perPage ) ; 
                console.log("lastPage" + lastPage);
                var limits = ` LIMIT ${perPage*(pageNumber-1)}, ${perPage}`;   
                
                const picQuery = "SELECT p.*, CAST(p.id AS CHAR) id_str FROM pictures p "  + conditions + limits;
                request.services.dbPool.query( 
                    picQuery,
                    queryParams,
                    ( err, results ) => {
                    if( err ) {
                        console.log( err ) ;
                        response.errorHandlers.send500() ;
                    } else {
                        response.setHeader( 'Content-Type', 'application/json' ) ;
                        console.log("total count of pages" + totalCnt);
                        response.end( JSON.stringify( {
                            meta: {
                                'total':       totalCnt,
                                'perPage':     perPage,
                                'currentPage': pageNumber,
                                'lastPage':    lastPage
                            },
                            data: results
                        } ) ) ;
                    }
                } ) ;
            }
        }
    ) ;    
}

function updatePicture( body ) {
    var picQuery = "UPDATE pictures SET " ;
    var picParams = [] ;
    var needComma = false ;
    for( let prop in body )
        if( prop != 'id' ) {
            if( needComma ) picQuery += ", " ;
            else needComma = true ;
            picQuery += prop + " = ? " ;
            picParams.push( body[prop] ) ;
        }
    picQuery += " WHERE id = ?";
    picParams.push( body.id ) ;
    
    return new Promise( (resolve, reject) => {
        global.services.dbPool.query(
            picQuery,
            picParams,
            (err, results) => {
                if( err ) reject( err ) ;
                else resolve( results ) ;
            } ) ;
        } ) ;
}

function validateOrm( body ) {
    return new Promise( (resolve, reject) => {
        validateId( body )
        .then( () => {
            const orm = [ "id", "title", "description", "place", "filename", "users_id", "upload_DT", "delete_DT" ] ;
            for( let prop in body ) {
                if( orm.indexOf( prop ) == -1 )
                    reject( "ORM error: unexpected field " + prop ) ;
            }
            resolve( body ) ;
        } )
        .catch( err => reject( err ) ) ;
    } ) ;    
}

function deletePicture( id, request ) {
    console.log("deletePicture");
    return new Promise( (resolve, reject) => {
    global.services.dbPool.query(
        "UPDATE pictures SET delete_DT = CURRENT_TIMESTAMP WHERE id = ?",
        id,
        (err, results) => {
            if( err ) reject( err ) ;
            else resolve( results ) ;
        } ) ;
    } ) ;
}

function validateId( body ) {
    return new Promise( (resolve, reject) => {
        console.log("body.id ", body);
        if ( ! body.id || ! /^\d+$/.test( body.id ) ) {  
            reject( "Id validation error" ) ;
        } else {
            resolve( body.id ) ;
        }
    } ) ;
}

function addPicture( pic, services ) {
    const query = "INSERT INTO pictures(title, description, place, filename, users_id) VALUES (?, ?, ?, ?, ?)" ;
    const params = [
        pic.title, 
        pic.description, 
        pic.place, 
        pic.filename,
        pic.users_id
    ] ;
    return new Promise( ( resolve, reject ) => {
        services.dbPool.query( query, params, ( err, results ) => {
            if( err )
            {
                reject( err ) ;
            } 
            else {
                  resolve( results ) ;
            }
        } ) ;
    } ) ;    
}

function moveUploadedFile( file ) {
    var counter = 1 ;
    var savedName ;
    do {
        savedName = `(${counter++})_${file.name}` ;
    } while( fs.existsSync( UPLOAD_PATH + savedName ) ) ;

    fs.copyFile( file.path, UPLOAD_PATH + savedName, 
        err => { if( err ) console.log( err ) ; } ) ;
    
    return savedName ;
}

function validatePictureForm( fields, files ) {
    // задание: проверить поля на наличие и допустимость
    if( typeof files["picture"] == 'undefined' ) {
        return "File required" ;
    }
    if( typeof fields["title"] == 'undefined' ) {
        return "Title required" ;
    }
    if( fields["title"].length == 0 ) {
        return "Title should be non-empty" ;
    }
    if( typeof fields["description"] == 'undefined' ) {
        return "Description required" ;
    }
    if( fields["description"].length == 0 ) {
        return "Description should be non-empty" ;
    }
    if( typeof fields["place"] != 'undefined'
     && fields["place"].length == 0 ) {
        return "Place should be non-empty" ;
    }
    if( typeof fields["users_id"] == 'undefined' ) {
        fields["users_id"] = null;
    }
    return true ;
}

function extractBody( request ) {
    return new Promise( ( resolve, reject ) => {
        let requestBody = [] ; 
        request
            .on( "data", chunk => requestBody.push( chunk ) )
            .on( "end", () => {
                try { 
                    resolve( JSON.parse( 
                        Buffer.concat( requestBody ).toString()
                    ) ) ;
                }
                catch( ex ) {
                    reject( ex ) ;
                }
           } ) ;
    } ) ;    
}
