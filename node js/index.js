// Сервер на JS
const HTTP_PORT    = 81 ;
const WWW_ROOT     = "www" ;
const FILE_404     = WWW_ROOT + "/404.html" ;
const DEFAULT_MIME = "application/octet-stream" ;
const UPLOAD_PATH  = WWW_ROOT + "/pictures/" ;
const MAX_SESSION_INTERVAL = 1000000 ;  // milliseconds

// Подключение модулей
const http       = require( "http" ) ;        // HTTP
const fs         = require( "fs" ) ;          // file system
const mysql      = require( 'mysql' ) ;
const crypto     = require( 'crypto' ) ;      // Средства криптографии (в т.ч. хеш)
const mysql2     = require( 'mysql2' ) ;      // Обновленные средства для MySQL

const pictureController = require( "./pictureController" ) ;

const connectionData = {
    host:     'localhost',     // размещение БД (возможно IP или hostname)
    port:     3306,            // порт 
    user:     'gallery_user',  // логин пользователя ( to 'gallery_user'@'localhost' )
    password: 'gallery_pass',  // пароль ( identified by 'gallery_pass' )
    database: 'gallery',       // schema/db  (  create database gallery; ) 
    charset:  'utf8'           // кодировка канала подключения
} ;

const services = { dbPool: mysql2.createPool( connectionData ) } ;
const sessions = {} ;
global.session = null ;

// Session cleaner
setInterval( () => {
    const moment = new Date() ;
    let expired = [] ;
    for( let index in sessions ) {
        if( moment - sessions[index].timestamp > MAX_SESSION_INTERVAL ) {
            expired.push( index ) ;
        }
    }
    for( let index of expired ) {
        if( global.session == sessions[ index ] ) global.session = null ;
        delete sessions[ index ] ;
    }
} , 1e4 ) ;


http.ServerResponse.prototype.send418 = async function() {
    this.statusCode = 418 ;
    this.setHeader( 'Content-Type', 'text/plain' ) ;
    this.end( "teapot" ) ;
} ;

// services.dbPool = mysql2.createPool( connectionData ) ;

// Серверная функция
function serverFunction( request, response ) {
    
    request.services = services ;
    global.services  = services ;

    response.errorHandlers = {
        "send500": () => {
            response.statusCode = 500 ;
            response.setHeader( 'Content-Type', 'text/plain' ) ;
            response.end( "Error in server" ) ;
        },
        "send412": message => {
            response.statusCode = 412 ;
            response.setHeader( 'Content-Type', 'text/plain' ) ;
            response.end( "Precondition Failed: " + message ) ;
        }
    } ;

    response.on( "close", () => {
        //services.dbPool.end() ;
    } ) ;

    request.params = { 
        body:  "",
        query: "",
        cookie: {}
    } ;
    analyze( request, response ) ;
}

function extractCookie( request ) {
    var res = {} ;
    if( typeof request.headers.cookie != 'undefined' ) {
        const cookies = request.headers.cookie.split( '; ' ) ;
        // name/value separated by '='
        for( let c of cookies ) {
            let pair = c.split( '=' ) ;
            if( typeof pair[0] != 'undefined'
             && typeof pair[1] != 'undefined' ) {
                res[ pair[0] ] = pair[1] ;
            }
        }    
    }
    return res ;
}

async function startSession( request ) {
    return new Promise( ( resolve, reject ) => {
        if( typeof request.params.cookie['session-id'] != 'undefined' ) { 
                const sessionId = request.params.cookie['session-id'] ;
        
            if( typeof sessions[ sessionId ] == 'undefined' ) {  
                global.services.dbPool.query(
                    "SELECT *, CAST(id AS CHAR) id_str FROM users WHERE id=?",
                    sessionId,
                    ( err, results ) => {
                        if( err ) {
                            console.log( "startSession " + err ) ;
                            global.session = null ;                            
                        } else {
                            sessions[ sessionId ] = {
                                'timestamp': new Date() ,
                                user: results[0]
                            } ;
                            global.session = sessions[ sessionId ] ;
                        }
                        resolve( global.session ) ;
                    }
                ) ;    
            } else {  
                global.session = sessions[ sessionId ] ;
                resolve( global.session ) ;
            }            
        } else {
            global.session = null ;
            resolve( global.session ) ;
        }    
    } ) ;    
}

async function analyze( request, response ) {
    console.log( request.method + " " + request.url ) ;
    
    var decodedUrl = request.url.replace( /\+/g, ' ' )  ;
    decodedUrl = decodeURIComponent( decodedUrl ) ;
    const requestParts = decodedUrl.split( "?" )  ;
    const requestUrl = requestParts[ 0 ] ;
    var params = {} ;
    if( requestParts.length > 1         
     && requestParts[1].length > 0 ) {  
        for( let keyval of requestParts[1].split( "&" ) ) {
            let pair = keyval.split( "=" ) ;
            params[ pair[0] ] = 
                typeof pair[1] == 'undefined'
                    ? null
                    : pair[1] ;
        }
    }
    request.params.query = params;
    request.params.cookie = extractCookie( request ) ;
    await startSession( request ) ;
    console.log( request.params.query, request.params.cookie/*, global.session */ ) ;  

    const restrictedParts = [ "../", ";" ] ;
    for( let part of restrictedParts ) {
        if( requestUrl.indexOf( part ) !== -1 ) {
            send418() ;
            return ;
        }
    }

    const path = WWW_ROOT + requestUrl ;
    if( fs.existsSync( path )   
     && fs.lstatSync( path ).isFile() ) {   
        sendFile( path, response ) ;
        return ;
    }

    const url = requestUrl.substring(1) ;
    request.decodedUrl = url ;
    if( url == '' ) {
        sendFile( WWW_ROOT + "/index.html", response ) ;
    }
    else if( url == 'db' ) {
        viewDb( request, response ) ;
    }
    else if( url == 'dbpool' ) {
        viewDbPool( request, response ) ;
    }
    else if( url == 'db2' ) {
        viewDb2( request, response ) ;
    }
    else if( url == 'auth' ) {
        viewAuth( request, response ) ;
    }
    else if( url == 'junk' ) {
        viewJunk( request, response ) ;
    }
    else if( url == 'download' ) {
        viewDownload( request, response ) ;
    }
    else if( url.indexOf( "api/" ) == 0 ) {     
        processApi( request, response ) ;
        return ;
    }
    else if( url == 'templates/auth.tpl' ) { 
        if( ! global.session || ! global.session.user ) {
            sendFile( WWW_ROOT + "/templates/auth_no.tpl", response ) ;
        } else {
            fs.readFile( WWW_ROOT + "/templates/auth_yes.tpl", (err, data) => {
                if( err ) {
                    console.log( "/templates/auth_yes.tpl : " + err ) ;
                    response.errorHandlers.send500() ;
                }
                response.end(
                    data.toString()
                    .replace('{{login}}', global.session.user.login )
                    .replace('{{id_str}}', global.session.user.id_str )
                ) ;
            } ) ;            
        }
    }
    else {
        sendFile( FILE_404, response, 404 ) ;  
    }
}

// Создание сервера (объект)
const server = http.createServer( serverFunction ) ;

// Запуск сервера - начало прослушивания порта
server.listen(  // регистрируемся в ОС на получение 
                // пакетов, адрессованных на наш порт 
    HTTP_PORT,  // номер порта
    () => {  // callback, после-обработчик, вызывается
             // после того, как "включится слушание"
        console.log( "Listen start, port " + HTTP_PORT ) ; 
    } 
) ;

async function viewDownload( request, response ) {
    global.services.dbPool.query(
        "SELECT filename FROM pictures WHERE id = ?",
        request.params.query.picid,
        (err, results) => {
            if(err) {console.log(err); response.errorHandlers.send500();}
            else {
                response.setHeader( 'Content-Type', 'application/octet-stream' ) ;
                fs.createReadStream( UPLOAD_PATH + results[0].filename )
                  .pipe( response ) ;
            }
        }
    ) ;
}


async function sendFile2( path, response, statusCode ) {
    fs.readFile(
        path,
        ( err, data ) => {
            if( err ) {
                console.error( err ) ;
                return;
            }
            if( typeof statusCode == 'undefined' )
                statusCode = 200 ;
            response.statusCode = statusCode ;
            response.setHeader( 'Content-Type', 'text/html; charset=utf-8' ) ;
            response.end( data ) ;
        } );
}

async function sendFile( path, response, statusCode=200 ) {
    var readStream = false ;
    if( fs.existsSync( path ) ) {
        readStream = fs.createReadStream( path ) ;      
    } else if( fs.existsSync( FILE_404 ) ) {
        readStream = fs.createReadStream( FILE_404 ) ;
        statusCode = 404 ;
    }    
    
    if( readStream ) {
        response.statusCode = statusCode ;
        response.setHeader( 'Content-Type', getMimeType( path ) ) ;
        readStream.pipe( response ) ;
    } else {
        response.statusCode = 418 ;
        response.setHeader( 'Content-Type', 'text/plain' ) ;
        response.end( "I'm a teapot" ) ;
    }

    // задание: проверить наличие файла перед отправкой:
    // 1. ищем файл, если есть - отправляем
    // 2. если нет - ищем 404, отправляем (если есть)
    // 3. если нет - отправляем строку с 418 кодом
}

// returns Content-Type header value by parsing file name (path)
function getMimeType( path ) {
    // file extension
    if( ! path ) {
        return false ;
    }
    const dotPosition = path.lastIndexOf( '.' ) ;
    if( dotPosition == -1 ) {  // no extension
        return DEFAULT_MIME ;
    }
    const extension = path.substring( dotPosition + 1 ) ;
    switch( extension ) {
        case 'html' :
        case 'css'  :
            return 'text/' + extension ;
        case 'jpeg' :
        case 'jpg'  :
            return 'image/jpeg' ;
        case 'bmp'  :
        case 'gif'  :
        case 'png'  :
            return 'image/' + extension ;
        case 'json' :
        case 'pdf'  :
        case 'rtf'  :
            return 'application/' + extension ;
        default :
            return DEFAULT_MIME ;
    }
}

// Обратка запросов   api/*
async function processApi( request, response ) {
    const apiUrl = request.decodedUrl.substring( 4 ) ;  // удаляем api/ из начала    
    const moduleName = "./" + apiUrl + "Controller.js" ;
    if( fs.existsSync( moduleName ) ) {
        import( moduleName )
        .then( ( { default: api } ) => {
            api.analyze( request, response ) ;
        } ) 
        .catch( console.log )
    } else {
        send418( response ) ;
    }
}

async function send418( response ) {
    // TODO: создать страницу "Опасный запрос"
    response.statusCode = 418 ;
    response.setHeader( 'Content-Type', 'text/plain' ) ;
    response.end( "I'm a teapot" ) ;
}

async function send500( response ) {
    response.statusCode = 500 ;
    response.setHeader( 'Content-Type', 'text/plain' ) ;
    response.end( "Error in server" ) ;
}

function viewDb( request, response ) {
    const connection = mysql.createConnection( connectionData ) ;
    
    connection.connect( err => {
        if( err ) {
            console.error( err ) ;
            send500( response ) ;
        } else {
            connection.query( "select * from users", ( err, results, fields ) => {
                if( err ) {
                    console.error( err ) ;
                    send500( response ) ;
                } else {
                    var table = "<table border=1>" ;
                    for( let row of results )
                        table += `<tr><td>${row.id}</td><td>${row.login}</td></tr>`
                    table += "</table>" ;
                    response.end( table ) ;
                }
            } ) ;           
        }
    } ) ;
}

function viewDbPool( request, response ) {
    const pool = mysql.createPool( connectionData ) ;
    pool.query( "select * from users", 
        ( err, results, fields ) => {
        if( err ) {
            console.error( err ) ;
            send500( response ) ;
        } else {
            var table = "<table border=1 cellspacing=0>" ;
            for( let row of results )
                table += `<tr><td>${row.id}</td><td>${row.login}</td><td>${row.email}</td></tr>`
            table += "</table>" ;
            response.end( table ) ;
        }
    } ) ;
}

function viewDb2( request, response ) {
    const pool2 = mysql2.createPool( connectionData ).promise() ;
    pool2.query( "select * from users" )
         .then( ( [ results, fields ] ) => {
            var table = "<table border=1 cellspacing=0>" ;
            for( let row of results )
                table += `<tr><td>${row.id}</td><td>${row.login}</td><td>${row.email}</td></tr>`
            table += "</table>" ;
            response.end( table ) ;
         } )
         .catch( err => { 
            console.error( err ) ;
            send500( response ) ;
         } )
}

function viewAuth( request, response ) {
    response.end(request.params.query.login + " " +request.params.query.pass  ) ;
}

function viewJunk( request, response ) {
    sendFile( WWW_ROOT + "/junk.html", response ) ;
}


