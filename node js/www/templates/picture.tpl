<div class="picture-item" id="{{id}}" picId="{{id}}">
    <div class="vote">
        <div class="vote-like">&#8593;</div>
        <div class="vote-total">0</div>
        <div class="vote-dislike">&#8595;</div>
    </div>
    <div class="picture-content" picId="{{picId}}">
        <img src="/pictures/{{filename}}" />
        <div>Название: <b>{{title}}</b></div>
        <div>Описание: <p>{{description}}</p></div>
        <div>Расположение: <i>{{place}}</i></div>
        <section class="tool-buttons">
            <span class="tb-delete"></span>
            <span class="tb-edit"></span>
            <span class="tb-download"></span>
        </section>
        <section class="tool-comments">
            <span class="picture-addcomment"></span>
            <span class="picture-showcomment"></span>
        </section>
        
        <div class="comments"></div>
    </div>
</div>