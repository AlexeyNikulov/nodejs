document.addEventListener("DOMContentLoaded",()=>{
    fetch("/api/picture?deleted")
    .then(r=>r.text())
    .then(t=>{
        const j = JSON.parse(t);
        const cont = document.getElementById("gallery-container");
        fetch("/templates/picture_junk.tpl").then(r=>r.text()).then(tpl=>{
            var html = "";
            for(let p of j){
                html += tpl.replace("{{id}}",p.id_str)
                           .replace("{{title}}",p.title)
                           .replace("{{description}}",p.description)
                           .replace("{{place}}",p.place)
                           .replace("{{filename}}",p.filename);
            }
            cont.innerHTML = html;
            addToolbuttonListeners();
        });
    });
});

async function addToolbuttonListeners() {
    for(let b of document.querySelectorAll(".tb-recover"))
        b.addEventListener("click",tbRecover);
}
function tbRecover(e) {
    const div = e.target.closest("div");
    const picId = div.getAttribute("picId");
    console.log(picId);
    fetch("/api/picture",{
        method: "put",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            id: picId,
            delete_DT: null
        })
    }).then(r=>r.json()).then(console.log);
}